<?php
namespace SlackErrorNotifier\Request;

use SlackErrorNotifier\Service\Transport\TransportType;

class ErrorRequest extends BaseRequest
{
    protected $requestUri = '';

    /**
     * ErrorRequest constructor.
     * @param $server
     * @param $filename
     * @param $LineNumber
     * @param $title
     * @param $description
     * @param $requestUri
     */
    public function __construct($server, $filename, $LineNumber, $title, $description, $requestUri)
    {
        parent::__construct($server, $filename, $LineNumber, $title, $description);
        $this->requestUri = $requestUri;
    }

    /**
     * @param $type
     * @return string
     */
    public function getMessage($type)
    {
        if ($type == TransportType::TELEGRAM) {
            return "📌 <b>Server:</b> <code>$this->server</code>\n"
                . "❌ <b>Error PHP in file:</b> <pre>$this->filename</pre>\n"
                . "📍 <b>At line:</b> <code>$this->lineNumber</code>\n"
                . "⚠️ <b>Type error:</b> <pre>$this->title: $this->description</pre>\n"
                . "🔗 <b>In:</b> <code>$this->requestUri</code>";
        }

        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Error PHP in file*: _' . $this->filename . ' *at line* : ' . $this->lineNumber . ' _'.
            ' *with type error:* ```' . $this->title . ': ' . $this->description . '```' . "\r\n" .
            '*in* _"' . $this->requestUri . '"_';
    }

}
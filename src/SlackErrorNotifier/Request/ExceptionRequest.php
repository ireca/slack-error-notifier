<?php
namespace SlackErrorNotifier\Request;


use SlackErrorNotifier\Service\Transport\TransportType;

class ExceptionRequest extends BaseRequest
{
    /**
     * @param $type
     * @return string
     */
    public function getMessage($type)
    {
        if ($type == TransportType::TELEGRAM) {
            return "📌 <b>Server:</b> <code>$this->server</code>\n"
                . "❌ <bException PHP in file:</b> <pre>$this->filename</pre>\n"
                . "📍 <b>At line:</b> <code>$this->lineNumber</code>\n"
                . "⚠️ <b></b>Message:</b> <pre>$this->title: $this->description</pre>\n"
                . "🔗 <b>Trace:</b> <code>$this->description</code>";
        }

        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Exception PHP in file*: ' . $this->filename . "\r\n" .
            '*At line* : ' . $this->lineNumber . "\r\n" .
            '*Message:* ' . $this->title . "\r\n" .
            '*Trace:* ```' . $this->description . '```';
    }
}
<?php
namespace SlackErrorNotifier\Request;

use SlackErrorNotifier\Service\Transport\TransportType;

class LogRequest extends BaseRequest
{
    protected $logViewerUrl = '';

    /**
     * LogRequest constructor.
     * @param $server
     * @param $description
     * @param $logViewerUrl
     * @param $createdAt
     */
    public function __construct($server, $description, $logViewerUrl)
    {
        parent::__construct($server, '', '', '', $description);
        $this->logViewerUrl = $logViewerUrl;
    }

    /**
     * @param $type
     * @return string
     */
    public function getMessage($type)
    {
        if ($type == TransportType::TELEGRAM) {
            return "📌 <b>Server:</b> <code>$this->server</code>\n"
                . "❌ <b> Log ID:</b> <pre>$this->logViewerUrl</pre>\n"
                . "🔗 <b>Description:</b> <code>$this->description</code>";
        }

        return
            '>>>*Server:* ' . $this->server . "\r\n" .
            '*Log ID*: ' . $this->logViewerUrl . "\r\n" .
            '*Description:* _' . $this->description . '_' ;
    }

}
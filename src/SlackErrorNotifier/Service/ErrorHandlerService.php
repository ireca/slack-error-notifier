<?php
namespace SlackErrorNotifier\Service;

use SlackErrorNotifier\Service\Transport\NotifierInterface;

class ErrorHandlerService
{
    /**
     * @var array
     */
    private $ignoreErrors = array();

    /**
     * @var NotifierInterface|null
     */
    private $notifierService = null;

    /**
     * ErrorHandlerService constructor.
     * @param NotifierInterface $apiNotifierService
     * @param array $ignoreErrors
     */
    public function __construct(NotifierInterface $apiNotifierService, array $ignoreErrors = array())
    {
        $this->notifierService = $apiNotifierService;
        $this->ignoreErrors = $ignoreErrors;
    }

    /**
     * Инициализация обработчика ошибок.
     *
     * @return $this
     */
    public function init()
    {
        // Регистрирует функцию, которая выполнится по завершении работы скрипта.
        register_shutdown_function(array($this, 'shutdownErrorPersonalHandler'));
        // Задает определенный пользователем обработчик ошибок.
        set_error_handler(array($this, 'errorPersonalHandler'));
        // Задает определенный пользователем обработчик не обработанных исключений.
        set_exception_handler(array($this, 'exceptionPersonalHandler'));

        return $this;
    }

    /**
     * Обрабатывает ошибки появившиеся в процессе работы и будет вызвана по завершению работы скрипта.
     */
    public function shutdownErrorPersonalHandler()
    {
        $eFatal = E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR;
        $error = error_get_last();
        if (! ($error && ($error['type'] & $eFatal))) {

            return true;
        }

        if ($this->isHandleError($error['type'], $error['message']) == false) {

            return true;
        }

        return $this
            ->getNotifierService()
            ->sendErrorNotify(
                $error['file'],
                $error['line'],
                $this->getPhpErrorTitleByErrorCode($error['type']),
                $error['message']
            );
    }

    /**
     * Обработчик ошибок, отправляет сообщение об ошибке в мессенджер Slack.
     *
     * @param $errorCode
     * @param $errorDescription
     * @param $errorFilename
     * @param $errorLineNumber
     * @return bool
     */
    public function errorPersonalHandler($errorCode, $errorDescription, $errorFilename, $errorLineNumber)
    {
        if ($this->isHandleError($errorCode, $errorDescription) == false) {

            return true;
        }

        return $this
            ->getNotifierService()
            ->sendErrorNotify(
                $errorFilename,
                $errorLineNumber,
                $this->getPhpErrorTitleByErrorCode($errorCode),
                $errorDescription
            );
    }

    /**
     * @param $errorCode
     * @param $errorReporting
     * @param $errorDescription
     * @return bool
     */
    private function isHandleError($errorCode, $errorDescription)
    {
        $errorReporting = E_ALL ^ (E_DEPRECATED | E_USER_DEPRECATED);
        if (! ($errorCode & $errorReporting)) {

            return false;
        }

        if ($this->isIgnoreError($errorDescription) == true) {

            return false;
        }

        return true;
    }

    /**
     * Обработчик исключений, отправляет сообщение об ошибке в мессенджер Slack.
     *
     * @param \Exception $ex
     * @return bool
     */
    public function exceptionPersonalHandler(\Exception $ex)
    {
        return $this
            ->getNotifierService()
            ->sendExceptionNotify($ex);
    }

    /**
     * @param $errorDescription
     * @return bool
     */
    private function isIgnoreError($errorDescription)
    {
        foreach ($this->ignoreErrors as $ignoreError) {
            if (stripos($errorDescription, $ignoreError) !== false) {

                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает сообщение об ошибке по коду ошибки.
     *
     * @param $code
     * @return string
     */
    private function getPhpErrorTitleByErrorCode($code)
    {
        $errorTitle = 'UNKNOWNED';
        switch ($code) {
            case E_ERROR:
                $errorTitle = 'E_ERROR';
                break;
            case E_WARNING:
                $errorTitle = 'E_WARNING';
                break;
            case E_PARSE:
                $errorTitle = 'E_PARSE';
                break;
            case E_NOTICE:
                $errorTitle = 'E_NOTICE';
                break;
            case E_CORE_ERROR:
                $errorTitle = 'E_CORE_ERROR';
                break;
            case E_CORE_WARNING:
                $errorTitle = 'E_CORE_WARNING';
                break;
            case E_COMPILE_ERROR:
                $errorTitle = 'E_COMPILE_ERROR';
                break;
            case E_COMPILE_WARNING:
                $errorTitle = 'E_COMPILE_WARNING';
                break;
            case E_USER_ERROR:
                $errorTitle = 'E_USER_ERROR';
                break;
            case E_USER_WARNING:
                $errorTitle = 'E_USER_WARNING';
                break;
            case E_USER_NOTICE:
                $errorTitle = 'E_USER_NOTICE';
                break;
            case E_STRICT:
                $errorTitle = 'E_STRICT';
                break;
            case E_RECOVERABLE_ERROR:
                $errorTitle = 'E_RECOVERABLE_ERROR';
                break;
            case E_DEPRECATED:
                $errorTitle = 'E_DEPRECATED';
                break;
            case E_USER_DEPRECATED:
                $errorTitle = 'E_USER_DEPRECATED';
                break;
        }

        return $errorTitle;
    }

    /**
     * @return NotifierInterface|null
     */
    public function getNotifierService()
    {
        return $this->notifierService;
    }

}
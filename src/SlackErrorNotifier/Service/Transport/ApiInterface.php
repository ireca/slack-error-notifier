<?php
namespace SlackErrorNotifier\Service\Transport;


interface ApiInterface
{
    /**
     * Отправляет сообщение в slack.
     *
     * @param $message
     * @param string $emoji
     * @param string $chanel
     * @return bool
     */
    public function sendNotify($message, $emoji = '', $chanel = '');

    /**
     * @return string
     */
    public function getType();
}
<?php
namespace SlackErrorNotifier\Service\Transport;


interface NotifierInterface
{
    public function sendMessage($chanel, $message, $emoji = '');

    public function sendErrorNotify($filename, $lineNumber, $title, $description, $emoji = ':exclamation:', $chanel = '');

    public function sendExceptionNotify(\Exception $ex, $emoji = ':collision:', $chanel = '');

    public function sendLogNotify($logId, $description, $emoji = ':grey_exclamation:', $chanel = '');
}
<?php

namespace SlackErrorNotifier\Service\Transport;

use Raven_Client;

class SentryApiService implements ApiInterface
{
    /**
     * @var Raven_Client
     */
    private $client = null;

    private $isActive = true;

    /**
     * @param array $options
     * @param $isActive
     * @throws \Raven_Exception
     */
    public function __construct(array $options, $isActive) {
        $this->client = (new Raven_Client($options))->install();
        $this->isActive = $isActive;
    }

    /**
     * @param $message
     * @param $emoji
     * @param $chanel
     * @return bool
     */
    public function sendNotify($message, $emoji = '', $chanel = '')
    {
        if ($this->isActive == false) {

            return true;
        }

        return ! is_null($this->client->captureMessage($message, array('emoji' => $emoji, 'chanel' => $chanel)));
    }

    /**
     * @return string
     */
    public function getType()
    {
        return TransportType::SENTRY;
    }

}
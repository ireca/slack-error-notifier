<?php
namespace SlackErrorNotifier\Service\Transport;

class TransportType
{
    const SLACK = 'slack';
    CONST TELEGRAM = 'telegram';
    CONST SENTRY = 'sentry';
}
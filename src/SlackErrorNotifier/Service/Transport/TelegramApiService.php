<?php
namespace SlackErrorNotifier\Service\Transport;

class TelegramApiService implements ApiInterface
{
    private $hookUrl = '';

    private $botToken = '';

    private $chatId = '';

    private $isActive = true;

    /**
     * @param string $hookUrl
     * @param string $botToken
     * @param string $chatId
     * @param bool $isActive
     */
    public function __construct($hookUrl, $botToken, $chatId, $isActive)
    {
        $this->hookUrl = $hookUrl;
        $this->botToken = $botToken;
        $this->chatId = $chatId;
        $this->isActive = $isActive;
    }

    /**
     * Отправляет сообщение в slack.
     *
     * @param $message
     * @param string $emoji
     * @param string $chanel
     * @return bool
     */
    public function sendNotify($message, $emoji = '', $chanel = '')
    {
        $isSent = true;
        if ($this->isActive == false) {

            return true;
        }

        $fields = array(
            'chat_id' => $this->chatId,
            'text' => str_replace('&', '%26', $message),
            "parse_mode" => "HTML"
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf($this->hookUrl, $this->botToken));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
        if (curl_exec($curl) === false) {
            $isSent = false;
        }
        curl_close($curl);

        return $isSent;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return TransportType::TELEGRAM;
    }

}
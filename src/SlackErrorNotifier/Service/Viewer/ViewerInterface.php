<?php
namespace SlackErrorNotifier\Service\Viewer;

interface ViewerInterface
{
    public function getLink($logId);

}
<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\ErrorHandlerService;

class ErrorHandlerFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return ErrorHandlerService
     * @throws \Raven_Exception
     */
    public static function createByConfig(array $config)
    {
        return new ErrorHandlerService(
            ErrorNotifierFactory::createByConfig($config),
            isset($config['ignoreErrors']) ? $config['ignoreErrors'] : array()
        );
    }
    
}
<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\Transport\TelegramApiService;

class TelegramNotifierFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return TelegramApiService
     */
    public static function createByConfig(array $config)
    {
        return new TelegramApiService(
            $config['params']['hookUrl'],
            $config['params']['botToken'],
            $config['params']['chatId'],
            $config['isActive']
        );
    }

    /**
     * @param $hookUrl
     * @param $botToken
     * @param $chatId
     * @param $isActive
     * @return TelegramApiService
     */
    public static function create($hookUrl, $botToken, $chatId, $isActive)
    {
        return new TelegramApiService($hookUrl, $botToken, $chatId, $isActive);
    }

}
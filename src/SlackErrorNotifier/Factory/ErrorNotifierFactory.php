<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\Transport\NotifierService;
use SlackErrorNotifier\Service\Transport\SentryApiService;
use SlackErrorNotifier\Service\Transport\SlackApiService;
use SlackErrorNotifier\Service\Transport\TelegramApiService;
use SlackErrorNotifier\Service\Viewer\BrowserViewerService;

class ErrorNotifierFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return NotifierService|null
     * @throws \Raven_Exception
     */
    public static function createByConfig(array $config)
    {
        return new NotifierService(
            $config['errorHandler']['server'],
            self::getApiService($config),
            isset($config['errorHandler']['viewer']) ? self::getViewerService($config) : null
        );
    }

    /**
     * @param array $config
     * @return SlackApiService|SentryApiService|TelegramApiService|null
     * @throws \Raven_Exception
     */
    private static function getApiService(array $config)
    {
        if ($config['errorHandler']['transport']['type'] == 'slack') {
            return new SlackApiService(
                $config['errorHandler']['transport']['params']['hookUrl'],
                $config['errorHandler']['transport']['params']['botName'],
                $config['errorHandler']['transport']['params']['chanel'],
                isset($config['errorHandler']['transport']['isActive'])
                    ? $config['errorHandler']['transport']['isActive']
                    : true
            );
        } else
        if ($config['errorHandler']['transport']['type'] == 'sentry') {
            $config['errorHandler']['transport']['params']['name'] = $config['errorHandler']['server'];
            return new SentryApiService(
                $config['errorHandler']['transport']['params'],
                isset($config['errorHandler']['transport']['isActive'])
                    ? $config['errorHandler']['transport']['isActive']
                    : true
            );
        } else
        if ($config['errorHandler']['transport']['type'] == 'telegram') {
            return new TelegramApiService(
                $config['errorHandler']['transport']['params']['hookUrl'],
                $config['errorHandler']['transport']['params']['botToken'],
                $config['errorHandler']['transport']['params']['chatId'],
                isset($config['errorHandler']['transport']['isActive'])
                    ? $config['errorHandler']['transport']['isActive']
                    : true
            );
        }

        return null;
    }

    /**
     * @param $config
     * @return BrowserViewerService|null
     */
    private static function getViewerService($config)
    {
        if ($config['errorHandler']['viewer']['type'] == 'browser') {
            return new BrowserViewerService(
                $config['errorHandler']['viewer']['params']['url'],
                $config['errorHandler']['viewer']['params']['urlParams']
            );
        }

        return null;
    }

}
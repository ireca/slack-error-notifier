<?php
namespace SlackErrorNotifier\Factory;

use SlackErrorNotifier\Service\Transport\SentryApiService;

class SentryNotifierFactory implements FactoryInterface
{
    /**
     * @param array $config
     * @return SentryApiService
     * @throws \Raven_Exception
     */
    public static function createByConfig(array $config)
    {
        return new SentryApiService($config['params'], $config['isActive']);
    }

    /**
     * @param array $options
     * @param $isActive
     * @return SentryApiService
     * @throws \Raven_Exception
     */
    public static function create(array $options, $isActive)
    {
        return new SentryApiService($options, $isActive);
    }
}
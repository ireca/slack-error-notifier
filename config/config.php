<?php
return array(
    'errorHandler' => array(
        'server' => 'server-name',
        'transport' => array(
            'type' => 'slack',
            'isActive' => true,
            'params' => array(
                'hookUrl' => 'https://hooks.slack.com',
                'botName' => 'botName',
                'chanel' => '#chanel',
            ),
            /*
                'type' => 'telegram',
                'isActive' => true,
                'params' => array(
                    'hookUrl' => 'https://api.telegram.org/bot%s/sendMessage',
                    'botToken' => '',
                    'chatId' => '',
                ),
             */

            /*
                'type' => 'sentry',
                'isActive' => true,
                'params' => array(
                    "dsn" => "http://public@localhost:9000/3",
                    "secret_key" => null,
                    "public_key" => null,
                    "project" => 1,
                    "auto_log_stacks" => true,
                    "tags" => array(),
                    "release" => "",
                    "environment" => null,
                    "sample_rate" => 1,
                    "trace" => true,
                    "timeout" => 2,
                    "message_limit" => 1024,
                    "exclude" => array(),
                    "excluded_exceptions" => array(),
                    "ignore_server_port" => false,
                    "extra" => array(),
                    "send_callback" => null,
                    "verify_ssl" => true,
           */
        ),
        // Set null if you don't want using log viewer
        'viewer' => array(
            'type' => 'browser',
            'params' => array(
                'url' => 'http://error-statistic.net/',
                'params' => array(
                    "logId" => ":logId",
                    "serverName" => "server-name"
                ),
            ),
        ),
    ),
    'ignoreErrors' => array(
        'stat()'
    ),
);
# Slack Error Notifier - Sends notifies about mistakes, exceptions and log messages to your slack account by using [Slack API]((https://api.slack.com/messaging/sending))

Slack Error Notifier - is a library that allows you to send error messages to you Slack account. 
This decision really useful when you using production server and needed get error messages in real time.

## Installation

Install the latest version with

```bash
$ composer require antonioo83/slack-error-notifier
```

## Basic Usage

```php
<?php

use \SlackErrorNotifier\Factory\ErrorHandlerFactory;

$config = array(
    'errorHandler' => array(
        'server' => 'server-name',
        'transport' => array(
            'type' => 'slack',
            'params' => array(
                'hookUrl' => 'https://hooks.slack.com',
                'botName' => 'botName',
                'chanel' => '#chanel',
            )
        ),
        // Set ('viewer' => null) if you don't want using log viewer
        'viewer' => array(
            'type' => 'browser',
            'params' => array(
                'url' => 'http://error-statistic.net/',
                'params' => array(
                    "logId" => ":logId",
                    "serverName" => "server-name"
                ),
            ),
        ),
    ),
    'ignoreErrors' => array(
        'stat()'
    ),
);
$service = ErrorHandlerFactory::createByConfig($config);
$service->init();

// Get an error!
$a = 10/0;
// or
throw new Exception('Test!');
// or
try {
 throw new Exception('Log exception');
} catch (\Exception $ex) {
  $id = writeLog($ex->getMessage());
  $service->getNotifierService()->sendLogNotify($id, $ex->getMessage());
}

// After that you will see next result in you Slack account
/*
Server: server-name
Error PHP in file: index.php at line : 31 with type error:
E_WARNING: Division by zero
in "/request/test"

// or

Server: server-name
Exception PHP in file: index.php
At line : 33
Message: Test!
Trace: ...

// or

 Server: server-name
 Log ID: 56 // 56 - link to your log viewer 
 Description: Log exception
 */
```

## Documentation

- [Slack API](https://api.slack.com/messaging/sending)

### Requirements

- Slack Error Notifier works with PHP 5.3 or above.

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/dashboard/issues?assignee_username=antonioo83)

### Framework Integrations

- Frameworks and libraries using [PSR-4](https://www.php-fig.org/psr/psr-4/)
  can be used very easily with Slack Error Notifier.
- [Symfony](http://symfony.com);
- [Zend](https://framework.zend.com/);
- [Yii](https://www.yiiframework.com/) and other.

### Author

Anton Yurchenko - <antonioo83@mail.ru>

### License

Slack Error Notifier is licensed under the MIT License - see the `LICENSE` file for details